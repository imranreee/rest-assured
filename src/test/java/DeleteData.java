
/*
 *  ****************************************************************************
 *  * Created by : Al Imran on 8/9/2018
 *  * Email : imranreee@gmail.com
 *  ****************************************************************************
 */

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteData {
    //For deleting any data
    @Test
    public void delete(){
        String apiUrl = "http://localhost:3000/posts/404";
        RequestSpecification request = RestAssured.given();
        Response response = request.delete(apiUrl);

        int responseCode = response.getStatusCode();
        System.out.println("Response code: "+responseCode);

        Assert.assertEquals(responseCode, 200);
        System.out.println("Successfully deleted");

    }

}
