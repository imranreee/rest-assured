
/*
 *  ****************************************************************************
 *  * Created by : Al Imran on 8/9/2018
 *  * Email : imranreee@gmail.com
 *  ****************************************************************************
 */

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetData {
    String apiLink = "https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22";

    @Test
    public void testResponseCode(){
        Response response = RestAssured.get(apiLink);

        //This part for checking status code
        int statusCode = response.getStatusCode();
        System.out.println("Status code is: "+statusCode);
        Assert.assertEquals(statusCode, 200);

        //For getting raw data as text
        String str = response.asString();
        System.out.println("Body is: "+str);

        //For getting response time
        System.out.println("Response time: " +response.getTime()+ " MS");
        System.out.println("A: " +response.contentType());




    }
}
