
/*
 *  ****************************************************************************
 *  * Created by : Al Imran on 8/9/2018
 *  * Email : imranreee@gmail.com
 *  ****************************************************************************
 */

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PostData {
    //For posting any data
    @Test
    public void post(){
        String apiUrl = "http://localhost:3000/posts";

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", 3);
        jsonObject.put("title", "RestAssured");
        jsonObject.put("author", "EzyAutomation");

        request.body(jsonObject.toJSONString());
        Response response = request.post(apiUrl);

        int responseCode = response.getStatusCode();
        System.out.println("Response code: "+responseCode);

        Assert.assertEquals(responseCode, 201);
        System.out.println("Successfully posted");
    }
}
