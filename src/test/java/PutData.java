
/*
 *  ****************************************************************************
 *  * Created by : Al Imran on 8/9/2018
 *  * Email : imranreee@gmail.com
 *  ****************************************************************************
 */

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PutData {
    //For updating any data(put request)
    @Test
    public void put(){
        String apiUrl = "http://localhost:3000/posts/1";

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", 1);
        jsonObject.put("title", "RestAssured");
        jsonObject.put("author", "EzyAutomation");

        request.body(jsonObject.toJSONString());
        Response response = request.put(apiUrl);

        int responseCode = response.getStatusCode();
        System.out.println("Response code: "+responseCode);

        Assert.assertEquals(responseCode, 200);
        System.out.println("Successfully updated");
    }
}
